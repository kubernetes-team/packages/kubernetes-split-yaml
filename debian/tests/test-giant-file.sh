#!/bin/bash
# autopkgtest check: Split and check a Kubernetes yaml manifest file against
# kubernetes-split-yaml, to verify that the file was splitted correctly.
# Author: Arthur Diniz <arthurbdiniz@gmail.com>

set -e

TEST_TMP="${AUTOPKGTEST_TMP:-/tmp}/kubernetes-split-yaml"

mkdir -p "${TEST_TMP}"
cp -r debian/tests/giant-file "${TEST_TMP}"
cd "${TEST_TMP}"

kubernetes-split-yaml giant-file/giant-k8s-file.yaml

if [ ! -f generated/my-nginx-deployment.yaml ]; then
  exit 1
fi

diff generated/my-nginx-deployment.yaml giant-file/my-nginx-deployment.yaml
echo "check-deployment: OK"

if [ ! -f generated/my-nginx-svc.yaml ]; then
  exit 1
fi

diff generated/my-nginx-svc.yaml giant-file/my-nginx-svc.yaml
echo "check-service: OK"